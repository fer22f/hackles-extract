#!/usr/bin/env python3
from bs4 import BeautifulSoup
from io import BytesIO
from PIL import Image
import requests
import locale
import re
import json

locale.setlocale(locale.LC_ALL, 'C')
import tesserocr

with open('data.json', 'a') as f:
    for comic_id in range(236, 365):
        response = requests.get(f'http://hackles.org/cgi-bin/archives.pl?request={comic_id}')
        soup = BeautifulSoup(response.text, 'lxml')

        date = soup.select_one('table:nth-child(1) .caption').text.strip()
        info = soup.select_one('table:nth-child(2) .caption').text.strip()
        image_src = 'http://hackles.org' + soup.select_one('img').attrs['src']

        img_response = requests.get(image_src)
        img = Image.open(BytesIO(img_response.content))

        img.save(f'comics/{comic_id}.{img.format}')

        text = tesserocr.image_to_text(img)

        text = re.sub('(?m)^.* \u00a9 \\d\\d\\d\\d .*\\n?', '', text)
        text = re.sub(r'(?m)^http://hackles.org\n?', '', text)
        text = re.sub(r'(?m)^By Drake Emko & Jen Brodzik\n?', '', text)
        text = re.sub(r'(?m)^Hackles\n?', '', text)
        text = re.sub(r'(?m)^ *\n', '', text)
        text = re.sub(r'\n\n', '\n', text)
        text = text.strip()

        obj = {
            'id': comic_id,
            'text': text,
            'date': date,
            'info': info,
            'image_src': image_src
        }

        f.write(json.dumps(obj) + '\n')

        print(json.dumps(obj))

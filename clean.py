#!/usr/bin/env python3
import json
import re
import shutil
import difflib
import sys
import os
from itertools import islice

with open('data_new.json', 'w') as data_new:
    with open('data.json') as data:
        for line in data.readlines():
            obj = json.loads(line)
            original_text = obj['text']
            obj['text'] = re.sub(r'(?m).*\d\d\d\d.*\n?', '', obj['text'])
            if original_text != obj['text']:
                text1 = original_text.splitlines(keepends=True)
                text2 = obj['text'].splitlines(keepends=True)
                sys.stdout.writelines(islice(difflib.unified_diff(text1, text2, n=0), 3, None))
                print()

            data_new.write(json.dumps(obj) + '\n')

confirm = input('Perform changes (y/n)? ')

if confirm == 'y':
    shutil.move('data.json', 'data_old.json')
    shutil.move('data_new.json', 'data.json')
else:
    os.remove('data_new.json')

with open('data.json') as data:
    for line in data.readlines():
        obj = json.loads(line)
        print(obj['text'])
